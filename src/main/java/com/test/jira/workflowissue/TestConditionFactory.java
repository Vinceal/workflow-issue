package com.test.jira.workflowissue;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import java.util.Map;

public class TestConditionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory {

    private MyBean bean;

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> map) {
        bean.test();
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {

    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {

    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> map) {
        return map;
    }

    public void setBean(MyBean bean) {
        this.bean = bean;
    }
}